# wsl-docker-gdk

My personal Setup for the gitlab-development-kit for WSL combined with Docker

## docker-sync

To get started with [docker-sync](https://docker-sync.readthedocs.io/en/latest/index.html) start up in the /docker-sync directory `docker-sync-stack start`. 

This will start the sync docker image and the GDK docker image. 

As soon the docker-sync setup is running you are able to access the GDK container with `docker exec -it gdk-run bash -l`.