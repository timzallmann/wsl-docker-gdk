#!/bin/bash

cd gitlab-development-kit
cp ./gitlab-copy/config/*.* ./gitlab/config/
cp -r gitlab-copy/node_modules gitlab/
cd gitlab
bundle install
